import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SessionComponent } from './../app/session/session.component';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

import { HttpModule } from '@angular/http';
import { SessionService } from './session/session.service';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'session', component: SessionComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    SessionComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )  
  ],
  providers: [SessionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
