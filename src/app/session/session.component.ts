import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SessionService } from './session.service';
import { SessionResponse, Round } from './SessionResponse';


@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {

  roundOwner: string;
  sectionRes: SessionResponse = null;
  totalETADate: Date = new Date();
  timerOperation: boolean;
  timerInterval: any;
  sectionBudgetInterval: any;
  activeMember: string;
  currentRoundIndex: number;
  budgetTimer: number;
  currentSectionIndex: number;
  totalETABudget: number;
  sectionBudget: number = 0;
  timer: number = 0;

  timeTakenByMembers: Round[] = [];

  constructor(private sessionService: SessionService) { }

  ngOnInit() {

    // get Ajax call
    this.sessionService.getSectionData()
      .subscribe(sectionData => this.init(sectionData));
    // this.init();
  }

  init(sectionData: SessionResponse) {

    this.sectionRes = sectionData;
    let date: Date = new Date();
    date.setMinutes(date.getMinutes() + this.convertIntoTime(sectionData.totalETA)[0]);
    this.totalETADate = date;

    this.totalETABudget = 0;
    this.currentSectionIndex = 0;
    this.startNextSection();
  }

  startNextSection() {
    this.currentRoundIndex = 0;
    this.timer = 0;
    this.budgetTimer = 0;
    this.timeTakenByMembers = [];
    this.onNextMember();
    this.stopSectionTimer();
    if (this.currentSectionIndex < this.sectionRes.sections.length) {
      this.sectionBudget = this.sectionRes.sections[this.currentSectionIndex].eta;
      this.startSectionTimer();
      this.roundOwner = this.sectionRes.sections[this.currentSectionIndex].roundOwner;
    }
  }
  
  startTimer() {
    this.timerInterval = setInterval(() => {
      this.timer--;
    }, 1000);
  }
  
  stopTimer() {
    clearInterval(this.timerInterval);
  }
  
  startSectionTimer() {
    this.sectionBudgetInterval = setInterval(() => {
      this.sectionBudget--;
    }, 1000);
  }

  stopSectionTimer() {
    clearInterval(this.sectionBudgetInterval);
  }

  convertIntoTime(valueInSec: number): number[] {
    const min: number = parseInt((valueInSec / 60).toString());
    const sec: number = valueInSec - (min * 60);
    return [Math.abs(min), Math.abs(sec)];
  }

  onTimerOpeation() {
    this.timerOperation = !this.timerOperation;
    if (!this.timerOperation) {
      this.stopTimer();
    } else {
      this.startTimer();
    }
  }

  onNextMember() {
    this.stopTimer();
    this.budgetTimer = this.budgetTimer + this.timer;
    if (this.currentSectionIndex < this.sectionRes.sections.length &&
      this.currentRoundIndex >= this.sectionRes.sections[this.currentSectionIndex].rounds.length) {
      // Next Section
      this.totalETABudget = this.totalETABudget + this.budgetTimer;
      let min = this.convertIntoTime(this.totalETABudget)[0];
      min = min * (this.totalETABudget >= 0 ? 1 : -1);
      this.totalETADate.setMinutes(this.totalETADate.getMinutes() + min);
      this.currentSectionIndex++;
      this.startNextSection();

    } else if (this.currentSectionIndex >= this.sectionRes.sections.length) {
      // finish
      // this.currentSectionIndex--;
    } else {
      if(this.currentRoundIndex > 0) {
        // save time taken by member
        this.timeTakenByMembers.push({
          memberName : this.sectionRes.sections[this.currentSectionIndex].rounds[this.currentRoundIndex].memberName,
          budget: this.timer
        });
      }
      this.timer = this.sectionRes.sections[this.currentSectionIndex].rounds[this.currentRoundIndex].budget;
      this.timerOperation = true;
      this.activeMember = this.sectionRes.sections[this.currentSectionIndex].rounds[this.currentRoundIndex].memberName;
      this.startTimer();
      this.currentRoundIndex++;
    }
  }

  get budgetTimerStr() {
    return this.formatToStr(this.budgetTimer, this.convertIntoTime(this.budgetTimer), true);
  }

  get timerStr() {
    return this.formatToStr(this.timer, this.convertIntoTime(this.timer), false);
  }

  get totalETAStr() {
    return this.formatToStr(1, [this.totalETADate.getHours(), this.totalETADate.getMinutes()], false);
  }

  get totalETABudgetStr() {
    return this.formatToStr(this.totalETABudget, this.convertIntoTime(this.totalETABudget), true);
  }

  get currentSection() {
    return (this.currentSectionIndex >= this.sectionRes.sections.length) ?
      this.sectionRes.sections.length : this.currentSectionIndex + 1;

  }

  get currentSectionName() {
    return (this.currentSectionIndex >= this.sectionRes.sections.length) ?
      this.sectionRes.sections[this.sectionRes.sections.length - 1].sectionName : this.sectionRes.sections[this.currentSectionIndex].sectionName;

  }

  get sectionBudgetStr() {
    return (this.formatToStr(this.sectionBudget, this.convertIntoTime(this.sectionBudget), true)) 
          + ' ' + ((this.sectionBudget < 0) ? 'LATE' : 'SAVE');
  }

  get totalRound() {
    return this.sectionRes.sections[this.currentSectionIndex].rounds.length;
  }

  get getSlowest1Member() {
    if(this.timeTakenByMembers.length > 1) {
      return 'P1 : 00:05';
    } else {
      return 'P1 : 00:05';
    }
  }
  
  formatToStr(timerValue: number, values: number[], isPlusSign: boolean): string {
    return ((timerValue < 0) ? '-' : (isPlusSign) ? '+' : '')
      + (values[0] < 10 ? '0' + values[0] : values[0]) + ':'
      + (values[1] < 10 ? '0' + values[1] : values[1]);
  }
}
