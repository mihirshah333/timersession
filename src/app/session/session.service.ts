import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SessionResponse } from './SessionResponse';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {
	url = "assets/data.json";
	constructor(private http: Http) { }

	getSectionData(): Observable<SessionResponse> {
		return this.http.get(this.url)
			.map(this.extractData)
			.catch(this.handleErrorObservable);
	}

	private extractData(res: Response) {
		let body = res.json();
		return body || {};
	}
	private handleErrorObservable(error: Response | any) {
		console.error(error.message || error);
		return Observable.throw(error.message || error);
	}
}