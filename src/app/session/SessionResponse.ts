export class SessionResponse {
  totalETA : number;
	sections : Section[];
}

export class Section {
	sectionName : string;
	eta : number;
	roundOwner : string;
	rounds: Round[];
}

export class Round {
	memberName : string;
	budget : number;
}